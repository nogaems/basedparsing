from basedparsing import Config, BaseParser
from basedparsing.sources import HTML
from pydantic import BaseModel
import pytest

import random


class Model(BaseModel):
    ...


class ParserWithoutTargets(BaseParser):
    @property
    async def targets(self):
        return []

    @property
    async def tts(self):
        return 0


class ParserWithTarget(BaseParser):
    @property
    async def targets(self):
        return [HTML('http://localhost:8080', self.cfg, self.logger)]

    @property
    async def tts(self):
        return 0


class ParserWithoutTTS(BaseParser):
    @property
    async def targets(self):
        return []

    @property
    async def tts(self):
        return 0


class ParserWithRandomTTS(BaseParser):
    @property
    async def targets(self):
        return []

    @property
    async def tts(self):
        return random.random()


@pytest.mark.asyncio
async def test_parser_without_targets():
    parser = ParserWithoutTargets(Config(), Model)
    assert len(await parser.targets) == 0


@pytest.mark.asyncio
async def test_parser_with_targets():
    parser = ParserWithTarget(Config(), Model)
    assert len(await parser.targets) == 1


@pytest.mark.asyncio
async def test_parser_without_tts():
    parser = ParserWithoutTTS(Config(), Model)
    assert await parser.tts == 0


@pytest.mark.asyncio
async def test_parser_with_random_tts():
    parser = ParserWithoutTTS(Config(), Model)
    assert await parser.tts >= 0
    assert await parser.tts < 1
