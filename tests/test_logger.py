from basedparsing import LoggerSetup, Config, BaseParser
from pydantic import BaseModel

import sys


class Parser(BaseParser):
    async def targets(self):
        return []

    async def tts(self):
        return 0


class Model(BaseModel):
    ...


no_logger = None
only_stderr = LoggerSetup(args=(sys.stderr,))
to_file = LoggerSetup(args=('log',), kwargs={'level': 'DEBUG'})


def test_no_logging():
    no_logging_parser = Parser(Config(use_logging=False), Model)
    assert len(
        no_logging_parser.logger._core.handlers) == 0


def test_no_loggers():
    no_loggers_parser = Parser(Config(), Model)
    handlers = no_loggers_parser.logger._core.handlers
    assert len(handlers) == 1
    stream = no_loggers_parser.logger._core.handlers[0]._sink._stream
    assert '_io.FileIO' in stream.name


def test_file_logging(tmp_path):
    path = tmp_path / 'test.log'
    loggers = [LoggerSetup(args=(path,))]
    file_logger_parser = Parser(Config(loggers=loggers), Model)
    handlers = file_logger_parser.logger._core.handlers
    assert len(handlers) == 1
    sink = handlers[0]._sink
    assert sink._path == str(path)


def test_file_with_level(tmp_path):
    path = tmp_path / 'test.log'
    loggers = [LoggerSetup(args=(path,), kwargs={'level': 'CRITICAL'})]
    file_logger_parser = Parser(Config(loggers=loggers), Model)
    handlers = file_logger_parser.logger._core.handlers
    assert len(handlers) == 1
    handler = handlers[0]
    sink = handler._sink
    assert sink._path == str(path)
    levelno = handler._levelno
    assert levelno == 50


def test_multiple_loggers(tmp_path):
    loggers = [
        LoggerSetup(args=(sys.stderr,)),
        LoggerSetup(args=(tmp_path / 'errors.log',),
                    kwargs={'level': 'ERROR'}),
        LoggerSetup(args=(tmp_path / 'debug.log',),
                    kwargs={'level': 'DEBUG', 'rotation': '500 MB'})
    ]
    multiple_loggers_parser = Parser(Config(loggers=loggers), model=Model)
    assert len(multiple_loggers_parser.logger._core.handlers) == 3
