import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="basedparsing",
    version="0.1.0",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    project_urls={
        "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'pydantic',
        'loguru',
        'aiohttp[speedups]'
    ],
    extras_require={
        'dev': [
            'flake8',
            'pytest',
            'pytest-cov',
            'pytest-asyncio'
        ]
    },
    packages=['basedparsing'],
    python_requires=">=3.8",
)
