from pydantic import BaseModel
from loguru._logger import Logger, Core

import abc
from typing import Union, Sequence
import sys

from .config import Config
from .sources import HTML, JSON


class BaseParser(abc.ABC):
    def __init__(self, config: Config, model: BaseModel) -> None:
        '''
        `config` contains settings for this particular parser.
        `model` is a model the output of the parser will be instantiated with.
        '''
        self.cfg = config
        self.model = model
        # These are the defaults from `loguru` code itself
        self.logger = Logger(core=Core(),
                             exception=None,
                             depth=0,
                             record=False,
                             lazy=False,
                             colors=False,
                             raw=False,
                             capture=True,
                             patcher=None,
                             extra={})

        if self.cfg.use_logging:
            if self.cfg.loggers:
                for entry in self.cfg.loggers:
                    self.logger.add(*entry.args, **entry.kwargs)
            else:
                self.logger.add(sys.stderr)

    @abc.abstractproperty
    async def targets(self) -> Sequence[Union[HTML, JSON]]:
        '''
        Should return a sequence of parsing targets. Often in order to construct a list of
        URLs to be parsed, it takes you to do some preparations, e.g. fetching another URL
        and parsing the response to extract pagination range. The code responsible for such
        actions must be placed in this method.
        '''
        raise NotImplementedError

    @abc.abstractproperty
    async def tts(self) -> int:
        '''
        Time to sleep between requests. Becomes useful when the server
        you send requests to has some sort of parsing prevention technique.
        Often it's either a random value in a predefined interval or a constant.
        Return 0 to not sleep at all.
        '''
        raise NotImplementedError
