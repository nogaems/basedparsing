from pydantic import BaseModel

from typing import Sequence, Optional, Dict, Any


class LoggerSetup(BaseModel):
    '''
    Settings that a loguru logger will be instantiated with.
    '''
    args: Sequence[Any]
    kwargs: Optional[Dict[str, Any]] = {}


class Config(BaseModel):
    '''
    `loggers` must be a sequence of `LoggerSetup` elements that will be be passed to
    [logger.add](https://loguru.readthedocs.io/en/stable/api/logger.html#loguru._logger.Logger.add)
    function, read the linked docs for more details about specific parameters.
    Providing no `loggers` is equal to setting it up to [LoggerSetup(args=(sys.stderr,))] and
    you can disable logging at all by setting `use_logging` to False.
    Here's an example of more complicated use case:
    ```
    loggers = [
        LoggerSetup(args=(sys.stderr,)),
        LoggerSetup(args=('errors.log',), kwargs={'level': 'ERROR'}),
        LoggerSetup(args=('debug.log',),  kwargs={'level': 'DEBUG', 'rotation': '500 MB'})
    ]
    ```
    In this setup it will simultaneously write everything that is below default loglevel to `stderr`,
    errors go to `errors.log` and extensive debug messages (including everything else) in a rotating at
    500 MB file `debug.log`.
    '''
    loggers: Sequence[LoggerSetup] = None
    use_logging: bool = True
    base_url: Optional[str]
    headers: Optional[Dict[str, str]]
