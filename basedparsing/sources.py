from loguru._logger import Logger

import abc

from .config import Config


class Source(abc.ABC):
    def __init__(self, url: str, config: Config, logger: Logger):
        self.url = url
        self.cfg = config
        self.logger = logger


class HTML(Source):
    ...


class JSON(Source):
    ...
